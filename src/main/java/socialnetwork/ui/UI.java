package socialnetwork.ui;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.PrietenieValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository0;
import socialnetwork.repository.database.MesajeDbRepo;
import socialnetwork.repository.database.PrietenieDbRepo;
import socialnetwork.repository.database.UtilizatorDbRepo;
import socialnetwork.service.ServiceMesaje;
import socialnetwork.service.ServicePrietenie;
import socialnetwork.service.ServicePrietenieUtilizator;
import socialnetwork.service.ServiceUtilizator;

import java.util.*;

public class UI {
    private final ServiceUtilizator srvUtilizator;
    private final ServicePrietenie srvPrietenie;
    private final ServicePrietenieUtilizator srvPrietenieUtilizator;
    private final ServiceMesaje srvMesaje;
    Scanner sc = new Scanner(System.in);

    public UI(ServiceUtilizator srvUtilizator, ServicePrietenie srvPrietenie, ServicePrietenieUtilizator srvPrietenieUtilizator, ServiceMesaje srvMesaje) {
        this.srvUtilizator = srvUtilizator;
        this.srvPrietenie = srvPrietenie;
        this.srvPrietenieUtilizator = srvPrietenieUtilizator;
        this.srvMesaje = srvMesaje;
    }

    static public void init(String[] args) {
        String userFile = "data/users.csv";
        String friendsFile = "data/prietenii.csv";
        try {
            Repository0<Long, Utilizator> repoUtilizator = new UtilizatorDbRepo(
                    args[0], args[1], args[2],
                    new UtilizatorValidator());
            Repository0<Tuple<Long, Long>, Prietenie> repoPrietenie = new PrietenieDbRepo(
                    args[0], args[1], args[2],
                    new PrietenieValidator());
            MesajeDbRepo repoMsgCoresp = new MesajeDbRepo(
                    args[0], args[1], args[2]);
            ServiceUtilizator srvUtilizatori = new ServiceUtilizator(repoUtilizator);
            ServicePrietenie srvPrietenie = new ServicePrietenie(repoPrietenie);
            ServiceMesaje srvMesaje = new ServiceMesaje(repoUtilizator, repoPrietenie,
                    repoMsgCoresp, srvUtilizatori, srvPrietenie);
            ServicePrietenieUtilizator srvUtilizatoriPrieteni =
                    new ServicePrietenieUtilizator(repoUtilizator, repoPrietenie, repoMsgCoresp, srvUtilizatori, srvPrietenie);
            UI ui = new UI(srvUtilizatori, srvPrietenie, srvUtilizatoriPrieteni, srvMesaje);
            ui.run();
        } catch (ValidationException ve) {
            System.out.println(ve.getMessage());
        }
    }

    private void listaComenzi() {
        System.out.println("""
                1-Add utilizator
                2-Update utilizator
                3-Remove utilizator
                9-Afiseaza toti utilizatorii
                10-Add prietenie
                11-Remove prietenie
                19-Afiseaza toate prieteniile
                20-Numar comunitati
                21-Cea mai sociabila comunitate
                31-Relatii de prietenie utilizator
                32-Adauga mesaj
                33-Afiseaza conversatie a doi utilizatori
                99-Afiseaza comenzi""");
    }

    private void addUtilizator() {
        System.out.println("Add utilizator");
        System.out.print("Id: ");
        Long id = sc.nextLong();
        sc.nextLine();
        System.out.print("firstName: ");
        String firstName = sc.nextLine();
        System.out.print("lastName: ");
        String lastName = sc.nextLine();
        srvUtilizator.addUtilizator(id, firstName, lastName);
        System.out.println("Utilizator adaugat!");
    }

    private void updUtilizator() {
        System.out.println("Update utilizator");
        System.out.print("Id: ");
        Long id = sc.nextLong();
        sc.nextLine();
        System.out.print("firstName: ");
        String firstName = sc.nextLine();
        System.out.print("lastName: ");
        String lastName = sc.nextLine();
        srvUtilizator.updateUtilizator(id, firstName, lastName);
        System.out.println("Utilizator modificat!");
    }

    private void remUtilizator() {
        System.out.println("Remove utilizator");
        System.out.print("Id: ");
        Long id = sc.nextLong();
        sc.nextLine();
        srvPrietenieUtilizator.deleteUtilizator(id);
        System.out.println("Utilizator sters!");
    }

    private void printUtilizatori() {
        System.out.println("Afiseaza utilizatori");
        srvUtilizator.findAll().forEach(System.out::println);
    }

    private void addPrietenie() {
        System.out.println("Add prietenie");
        System.out.print("Id1: ");
        Long id1 = sc.nextLong();
        System.out.print("Id2: ");
        Long id2 = sc.nextLong();
        srvPrietenieUtilizator.addPrietenie(id1, id2);
        System.out.println("Prietenie adaugata!");
    }

    private void remPrietenie() {
        System.out.println("Remove prietenie");
        System.out.print("Id1: ");
        Long id1 = sc.nextLong();
        System.out.print("Id2: ");
        Long id2 = sc.nextLong();
        srvPrietenie.deletePrietenie(id1, id2);
        System.out.println("Prietenie stearsa!");
    }

    private void printPrietenii() {
        System.out.println("Afiseaza prietenii");
        srvPrietenie.findAll().forEach(System.out::println);
    }

    private void numarComunitati() {
        int nrC = srvPrietenieUtilizator.numarComunitati();
        System.out.println("Numar de comunitati: " + nrC);
    }

    private void ComunitateSociabila() {
        List<Utilizator> rez = new ArrayList<>();
        Integer compS = srvPrietenieUtilizator.ComunitateSociabila(rez);
        System.out.println("Lungimea comunitatii cea mai sociabile: " + compS);
        System.out.println("Componenta cea mai sociabila:");
        rez.forEach(System.out::println);
    }

    private void relatiiUtilizator() {
        System.out.println("Relatii utilizator: ");
        System.out.print("Id: ");
        Long id = sc.nextLong();
        System.out.print("Luna: ");
        Integer luna = sc.nextInt();
        srvPrietenieUtilizator.relatiiUtilizator(id, luna).forEach(System.out::println);
    }

    private void adaugaMesaj() {
        System.out.println("Adauga mesaj: ");
        System.out.print("Id sender: ");
        Long idSender = sc.nextLong();
        sc.nextLine();
        System.out.print("Ids receivers: ");
        String ids = sc.nextLine();
        List<Long> idsReceivers = Arrays.stream(ids.split(","))
                .map(x -> Long.parseLong(x.strip())).toList();
        System.out.println("Mesaj: ");
        String msg = sc.nextLine();
        srvMesaje.adaugaMesaj(idSender, idsReceivers, msg);
    }

    private void afisareConversatie() {
        System.out.println("Afisare conversatie a doi utilizatori: ");
        System.out.print("Id1: ");
        Long id1 = sc.nextLong();
        System.out.print("Id2: ");
        Long id2 = sc.nextLong();
        srvMesaje.afisareConversatie(id1,id2).forEach(System.out::println);
    }


    public void run() {
        int caz = -1;
        listaComenzi();
        while (true) {
            System.out.print(">>>");
            try {
                caz = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Nu s-a introdus un intreg");
                continue;
            }
            if (caz == 0) {
                System.out.println("Iesire din program");
                break;
            }
            try {
                switch (caz) {
                    case 99 -> listaComenzi();
                    case 1 -> addUtilizator();
                    case 2 -> updUtilizator();
                    case 3 -> remUtilizator();
                    case 9 -> printUtilizatori();
                    case 10 -> addPrietenie();
                    case 11 -> remPrietenie();
                    case 19 -> printPrietenii();
                    case 20 -> numarComunitati();
                    case 21 -> ComunitateSociabila();
                    case 31 -> relatiiUtilizator();
                    case 32 -> adaugaMesaj();
                    case 33 -> afisareConversatie();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }

}
